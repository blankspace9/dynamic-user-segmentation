# dynamic-user-segmentation  
Сервис для динамического сегментирования пользователей.  
  
Используемые технологии:  
- PostgreSQL
- Docker
- gorilla/mux
- pq
- logrus
- viper
- testify, sqlmock
  
Сервис был написан с использованием чистой архитектуры. Также был реализован Graceful Shutdown для корректного завершения работы и были написаны unit тесты для слоя repository.

## Getting started

Запустить сервис можно с помощью команды `docker-compose up --build -d && docker-compose logs -f`  

## Examples  
#### Добавление сегмента  
Запрос
``````
curl --location 'localhost:8080/segment' \
--header 'Content-Type: application/json' \
--data '{
    "title": "VOICE_MESSAGES"
}
``````  
Ответ  
``````
{
    "id": 1
}
``````
#### Удаление сегмента  
Запрос
``````
curl --location --request DELETE 'localhost:8080/segment' \
--header 'Content-Type: application/json' \
--data '{
    "title": "VOICE_MESSAGES"
}
``````  
Ответ
``````
{
    "message": "success"
}
``````  
#### Добавление пользователя в сегменты  
Запрос
``````
curl --location 'localhost:8080/segment' \
--header 'Content-Type: application/json' \
--data '{
    "newSegments": ["VOICE_MESSAGES", "DISCOUNT_50"],
    "cancelledSegments": [],
    "userId": 1000,
    "ttl": "72h"
}
``````  
Ответ 
``````
{
    "message": "success"
}
``````  
#### Получение активных сегментов пользователя
Запрос
``````
curl --location --request GET 'localhost:8080/map' \
--header 'Content-Type: application/json' \
--data '{
    "userId": 1000
}'
``````  
Ответ 
``````
{
    "segments": [
        "DISCOUNT_50",
        "VOICE_MESSAGES"
    ]
}
``````  
#### Получение отчета
Запрос
``````
curl --location --request GET 'localhost:8080/report' \
--header 'Content-Type: application/json' \
--data '{
    "userId": 1000,
    "year": 2023,
    "month": 8
}'
``````  
Ответ 
``````
{
    "link": "http://localhost:8080/files/user1000-2023-8-4b902f4f-d625-4034-a7ea-abaa088e051a.csv"
}
``````  
Формат отчета
> Segment;Action;Datetime  
> VOICE_MESSAGES;add;2023-08-31 15:47:37  
> DISCOUNT_50;add;2023-08-31 15:47:37
