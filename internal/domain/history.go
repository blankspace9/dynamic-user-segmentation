package domain

import "time"

type HistoryLog struct {
	UserId  int64
	Segment string
	Action  string
	Date    time.Time
}

type HistoryRequest struct {
	UserId int64 `json:"userId"`
	Year   int   `json:"year"`
	Month  int   `json:"month"`
}
