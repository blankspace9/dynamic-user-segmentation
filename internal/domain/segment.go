package domain

type Segment struct {
	Id    int64  `json:"id"`
	Title string `json:"title"`
}
