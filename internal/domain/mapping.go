package domain

import "time"

type Mapping struct {
	UserId           int64    `json:"userId"`
	NewSegments      []string `json:"newSegments"`
	CanelledSegments []string `json:"cancelledSegments"`
	TTL              string   `json:"ttl"`
}

type MappingWithTime struct {
	UserId           int64
	NewSegments      []string
	CanelledSegments []string
	ExpiredAt        time.Time
}
