package config

import (
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/spf13/viper"
)

type (
	Config struct {
		DB Postgres

		Server struct {
			Port string `mapstructure:"port"`
		} `mapstructure:"server"`

		Log struct {
			Level string `mapstructure:"level"`
		} `mapstructure:"log"`

		TTL struct {
			Interval time.Duration `mapstructure:"interval"`
		} `mapstructure:"ttl"`
	}

	Postgres struct {
		Host     string
		Port     int
		Username string
		DB       string
		SSLMode  string
		Password string
	}
)

func NewConfig(folder, filename string) (*Config, error) {
	cfg := new(Config)

	if os.Getenv("DOCKER_ENV") != "true" {
		err := godotenv.Load(".env")
		if err != nil {
			return nil, err
		}
	}

	viper.AddConfigPath(folder)
	viper.SetConfigName(filename)

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}

	if err := envconfig.Process("postgres", &cfg.DB); err != nil {
		return nil, err
	}

	return cfg, nil
}
