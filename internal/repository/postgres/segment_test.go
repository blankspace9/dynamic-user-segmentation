package postgres

import (
	"context"
	"errors"
	"log"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

func TestSegmentPostgres_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	r := NewSegmentRepo(db)

	type args struct {
		listId int
		item   domain.Segment
	}
	type mockBehavior func(args args, id int64)

	testTable := []struct {
		name         string
		mockBehavior mockBehavior
		args         args
		id           int64
		wantErr      bool
	}{
		{
			name: "OK",
			args: args{
				listId: 1,
				item: domain.Segment{
					Title: "VOICE_MESSAGES",
				},
			},
			id: int64(2),
			mockBehavior: func(args args, id int64) {
				rows := sqlmock.NewRows([]string{"id"}).AddRow(id)
				mock.ExpectQuery("INSERT INTO segments").WithArgs(args.item.Title).WillReturnRows(rows)
			},
		},
		{
			name: "Empty fields",
			args: args{
				listId: 1,
				item: domain.Segment{
					Title: "",
				},
			},
			mockBehavior: func(args args, id int64) {
				rows := sqlmock.NewRows([]string{"id"}).AddRow(id).RowError(1, errors.New("some error"))
				mock.ExpectQuery("INSERT INTO segments").WithArgs(args.item.Title).WillReturnRows(rows)
			},
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			testCase.mockBehavior(testCase.args, testCase.id)

			got, err := r.Create(context.Background(), testCase.args.item)
			if testCase.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, testCase.id, got)
				t.Logf("Calling Create({Title: %d}), result %d", testCase.id, got)
			}
		})
	}
}

func TestSegmentPostgres_Delete(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	r := NewSegmentRepo(db)

	type args struct {
		listId int
		item   domain.Segment
	}
	type mockBehavior func(args args)

	testTable := []struct {
		name         string
		mockBehavior mockBehavior
		args         args
		id           int64
		wantErr      bool
	}{
		{
			name: "OK",
			args: args{
				listId: 1,
				item: domain.Segment{
					Title: "VOICE_MESSAGES",
				},
			},
			id: int64(1),
			mockBehavior: func(args args) {
				result := sqlmock.NewResult(1, 1)
				mock.ExpectExec("DELETE FROM segments").WithArgs(args.item.Title).WillReturnResult(result)
			},
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			testCase.mockBehavior(testCase.args)

			err := r.Delete(context.Background(), testCase.args.item.Title)
			if testCase.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
