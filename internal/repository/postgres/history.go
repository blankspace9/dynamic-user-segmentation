package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

type HistoryRepo struct {
	db *sql.DB
}

func NewHistoryRepo(db *sql.DB) *HistoryRepo {
	return &HistoryRepo{db}
}

func (hr *HistoryRepo) Create(ctx context.Context, historyLog domain.HistoryLog) error {
	_, err := hr.db.Exec("INSERT INTO mapping_history (user_id, segment_title, action, time) values ($1, $2, $3, $4)", historyLog.UserId, historyLog.Segment, historyLog.Action, historyLog.Date)
	if err != nil {
		fmt.Println("ERROR:", err)
		return err
	}

	return nil
}

func (hr *HistoryRepo) Get(ctx context.Context, req domain.HistoryRequest) ([]domain.HistoryLog, error) {
	var logs []domain.HistoryLog
	var log domain.HistoryLog
	rows, err := hr.db.Query("SELECT user_id, segment_title, action, time FROM mapping_history WHERE user_id=$1 AND EXTRACT(YEAR FROM time)=$2 AND EXTRACT(MONTH FROM time)=$3", req.UserId, req.Year, req.Month)
	if err != nil {
		fmt.Println("ERROR1:", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&log.UserId, &log.Segment, &log.Action, &log.Date); err != nil {
			fmt.Println("ERROR2:", err)
			return nil, err
		}
		logs = append(logs, log)
	}

	return logs, nil
}
