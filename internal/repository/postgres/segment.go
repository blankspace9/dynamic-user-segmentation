package postgres

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type SegmentRepo struct {
	db *sql.DB
}

func NewSegmentRepo(db *sql.DB) *SegmentRepo {
	return &SegmentRepo{db}
}

func (sr *SegmentRepo) Create(ctx context.Context, segment domain.Segment) (int64, error) {
	var id int64
	err := sr.db.QueryRow("INSERT INTO segments (title) values ($1) RETURNING id", segment.Title).Scan(&id)
	if err != nil {
		pqErr, ok := err.(*pq.Error)
		if ok && pqErr.Code == "23505" {
			return -1, repoerrors.ErrAlreadyExist
		}
		return -1, err
	}

	return id, nil
}

func (sr *SegmentRepo) Delete(ctx context.Context, title string) error {
	_, err := sr.db.Exec("DELETE FROM segments WHERE title=$1", title)
	if err != nil {
		return err
	}

	return err

}
