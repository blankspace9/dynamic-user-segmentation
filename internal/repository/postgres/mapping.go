package postgres

import (
	"context"
	"database/sql"
	"time"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type MappingRepo struct {
	db *sql.DB
}

func NewMappingRepo(db *sql.DB) *MappingRepo {
	return &MappingRepo{db}
}

func (mr *MappingRepo) Create(ctx context.Context, userId int64, segment string, ttl time.Duration) error {
	_, err := mr.db.Exec("INSERT INTO user_segment_mapping (user_id, segment_title, expired_at) values ($1, $2, $3)", userId, segment, time.Now().Add(ttl))
	if err != nil {
		pqErr, ok := err.(*pq.Error)
		if ok && pqErr.Code == "23505" {
			return repoerrors.ErrAlreadyExist
		}

		// Ignore non-existent segment
		if ok && pqErr.Code != "23503" {
			return err
		}
	}

	return nil
}

func (mr *MappingRepo) GetSegmentsByUser(ctx context.Context, userId int64) ([]string, error) {
	var segments []string

	rows, err := mr.db.Query("SELECT segment_title FROM user_segment_mapping WHERE user_id=$1", userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var segmentTitle string
		if err := rows.Scan(&segmentTitle); err != nil {
			return nil, err
		}
		segments = append(segments, segmentTitle)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	if len(segments) == 0 {
		return nil, repoerrors.ErrNotFound
	}

	return segments, nil
}

func (mr *MappingRepo) Delete(ctx context.Context, userId int64, segment string) error {
	_, err := mr.db.Exec("DELETE FROM user_segment_mapping WHERE user_id=$1 AND segment_title=$2", userId, segment)
	if err != nil {
		return err
	}

	return nil
}

func (mr *MappingRepo) DeleteExpired() error {
	currentTime := time.Now()
	_, err := mr.db.Exec("DELETE FROM user_segment_mapping WHERE expired_at<$1", currentTime)
	if err != nil {
		return err
	}

	return nil
}
