package postgres

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestMappingPostgres_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	r := NewMappingRepo(db)

	type args struct {
		listId  int
		userId  int64
		segment string
		ttl     time.Duration
	}
	type mockBehavior func(args args)

	testTable := []struct {
		name         string
		mockBehavior mockBehavior
		args         args
		wantErr      bool
	}{
		{
			name: "OK",
			args: args{
				listId:  1,
				userId:  1000,
				segment: "VOICE_MESSAGES",
				ttl:     72 * time.Hour,
			},
			mockBehavior: func(args args) {
				result := sqlmock.NewResult(1, 1)
				mock.ExpectExec("INSERT INTO user_segment_mapping").WithArgs(args.userId, args.segment, args.ttl).WillReturnResult(result)
			},
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			testCase.mockBehavior(testCase.args)
			err := r.Create(context.Background(), testCase.args.userId, testCase.args.segment, testCase.args.ttl)
			if testCase.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
