package postgres

import (
	"context"
	"log"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

func TestUserPostgres_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	r := NewUserRepo(db)

	type args struct {
		listId int
		item   domain.User
	}
	type mockBehavior func(args args, id int64)

	testTable := []struct {
		name         string
		mockBehavior mockBehavior
		args         args
		id           int64
		wantErr      bool
	}{
		{
			name: "OK",
			args: args{
				listId: 1,
				item: domain.User{
					Id: 1000,
				},
			},
			id: int64(1000),
			mockBehavior: func(args args, id int64) {
				rows := sqlmock.NewRows([]string{"id"}).AddRow(id)
				mock.ExpectQuery("INSERT INTO users").WithArgs(args.item.Id).WillReturnRows(rows)
			},
		},
	}

	for _, testCase := range testTable {
		t.Run(testCase.name, func(t *testing.T) {
			testCase.mockBehavior(testCase.args, testCase.id)

			got, err := r.Create(context.Background(), testCase.args.item)
			if testCase.wantErr {
				assert.Error(t, err)
			} else {
				t.Logf("Calling Create({id: %d}), result %d", testCase.id, got)
				assert.NoError(t, err)
				assert.Equal(t, testCase.id, got)
			}
		})
	}
}
