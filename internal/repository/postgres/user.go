package postgres

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type UserRepo struct {
	db *sql.DB
}

func NewUserRepo(db *sql.DB) *UserRepo {
	return &UserRepo{db}
}

func (ur *UserRepo) Create(ctx context.Context, user domain.User) (int64, error) {
	var id int64
	err := ur.db.QueryRow("INSERT INTO users (id) values ($1) RETURNING id", user.Id).Scan(&id)
	if err != nil {
		pqErr, ok := err.(*pq.Error)
		if ok && pqErr.Code == "23505" {
			return -1, repoerrors.ErrAlreadyExist
		}
		return -1, err
	}

	return id, nil
}
