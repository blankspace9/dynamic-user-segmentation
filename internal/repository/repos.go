package repository

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/postgres"
)

type User interface {
	Create(ctx context.Context, user domain.User) (int64, error)
}

type Segment interface {
	Create(ctx context.Context, segment domain.Segment) (int64, error)
	Delete(ctx context.Context, segmentTitle string) error
}

type Mapping interface {
	Create(ctx context.Context, userId int64, segment string, ttl time.Duration) error
	GetSegmentsByUser(ctx context.Context, userId int64) ([]string, error)
	Delete(ctx context.Context, userId int64, segmentTitle string) error
	DeleteExpired() error
}

type History interface {
	Create(ctx context.Context, historyLog domain.HistoryLog) error
	Get(ctx context.Context, req domain.HistoryRequest) ([]domain.HistoryLog, error)
}

type Repositories struct {
	User
	Segment
	Mapping
	History
}

func NewRepositories(db *sql.DB) *Repositories {
	return &Repositories{
		User:    postgres.NewUserRepo(db),
		Segment: postgres.NewSegmentRepo(db),
		Mapping: postgres.NewMappingRepo(db),
		History: postgres.NewHistoryRepo(db),
	}
}
