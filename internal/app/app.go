package app

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/config"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/delivery/rest"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/service"
	"gitlab.com/blankspace9/dynamic-user-segmentation/pkg/httpserver"
	"gitlab.com/blankspace9/dynamic-user-segmentation/pkg/postgres"
)

func Run(folder, filename string) {
	cfg, err := config.NewConfig(folder, filename)
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	SetLogger(cfg.Log.Level)

	log.Info("Init postgres db...")
	pg, err := postgres.NewPostgresConnection(postgres.PostgresConnectionInfo{
		Host:     cfg.DB.Host,
		Port:     cfg.DB.Port,
		Username: cfg.DB.Username,
		DBName:   cfg.DB.DB,
		SSLMode:  cfg.DB.SSLMode,
		Password: cfg.DB.Password,
	})
	if err != nil {
		log.Fatal(fmt.Errorf("Run app - Init postgres db: %w", err))
	}
	defer pg.Close()

	// Repositories
	log.Info("Initializing repositories...")
	repositories := repository.NewRepositories(pg)

	log.Info("Initializing dependencies...")
	deps := service.ServiceDependencies{
		Repos:            repositories,
		TTLCheckInterval: cfg.TTL.Interval,
		Port:             cfg.Server.Port,
	}

	log.Info("Initializing services...")
	services := service.NewServices(deps)

	log.Info("Starting TTL manager...")
	go services.TTLManager.Start()
	defer services.TTLManager.Stop()

	log.Info("Initializing handler...")
	handler := rest.NewHandler(*services)

	// init & run server
	log.Info("Starting http server...")
	log.Debugf("Server port: %s", cfg.Server.Port)
	httpServer := httpserver.NewServer(handler.InitRouter(), httpserver.Port(cfg.Server.Port))

	log.Info("Server started")

	// graceful shutdown
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-exit:
		log.Info("Run app - signal: " + s.String())
	case err = <-httpServer.Notify():
		log.Error(fmt.Errorf("Run app - httpServer.Notify: %w", err))
	}

	// Graceful shutdown
	log.Info("Shutting down...")
	err = httpServer.Shutdown()
	if err != nil {
		log.Error(fmt.Errorf("Run app - httpServer.Shutdown: %w", err))
	}

	log.Info("Server gracefully stopped")
}
