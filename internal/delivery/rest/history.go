package rest

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

func (h *Handler) getReport(w http.ResponseWriter, r *http.Request) {
	var historyRequest domain.HistoryRequest

	d := json.NewDecoder(r.Body)

	err := d.Decode(&historyRequest)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		logrus.Error("History - getReport - Failed to parse JSON:", err)
		return
	}

	if historyRequest.Year > time.Now().Year() || historyRequest.Month > 12 || (historyRequest.Year == time.Now().Year() && historyRequest.Month > int(time.Now().Month())) {
		http.Error(w, "Wrong date format", http.StatusBadRequest)
		logrus.Error("History - getReport - Wrong date format:", err)
		return
	}

	link, err := h.historyService.GetReport(r.Context(), historyRequest)
	if err != nil {
		http.Error(w, "Get logs error", http.StatusInternalServerError)
		logrus.Error("History - getReport - Get logs error:", err)
		return
	}

	response, err := json.Marshal(map[string]string{
		"link": link,
	})
	if err != nil {
		http.Error(w, "Failed to encode response JSON", http.StatusInternalServerError)
		logrus.Error("History - getReport - Failed to encode response JSON:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) getFile(w http.ResponseWriter, r *http.Request) {
	filename := mux.Vars(r)["filename"]
	file, err := os.Open("files/" + filename)
	if err != nil {
		http.Error(w, "Failed to open file", http.StatusInternalServerError)
		logrus.Error("History - getFile - Failed to open file:", err)
		return
	}

	w.Header().Set("Content-Type", "text/csv")

	_, err = io.Copy(w, file)
	if err != nil {
		http.Error(w, "Failed to copy file content", http.StatusInternalServerError)
		logrus.Error("History - getFile - Failed to copy file content:", err)
		return
	}

	file.Close()

	err = os.RemoveAll("files/" + filename)
	if err != nil {
		http.Error(w, "Failed to delete file", http.StatusInternalServerError)
		logrus.Error("History - getFile - Failed to delete file:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
