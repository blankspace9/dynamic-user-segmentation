package rest

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

func (h *Handler) getSegments(w http.ResponseWriter, r *http.Request) {
	var user domain.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		logrus.Error("Mapping - getSegments - Failed to parse JSON:", err)
		return
	}

	segments, err := h.mappingService.GetSegmentsByUser(r.Context(), user.Id)
	if err != nil {
		http.Error(w, "Get mapping error", http.StatusInternalServerError)
		logrus.Error("Mapping - getSegments - Get mapping error:", err)
		return
	}

	response, err := json.Marshal(map[string][]string{
		"segments": segments,
	})
	if err != nil {
		http.Error(w, "Failed to encode response JSON", http.StatusInternalServerError)
		logrus.Error("Mapping - getSegments - Failed to encode response JSON:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) mapSegments(w http.ResponseWriter, r *http.Request) {
	var mapping domain.Mapping

	err := json.NewDecoder(r.Body).Decode(&mapping)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		logrus.Error("Mapping - mapSegments - Failed to parse JSON:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "bad request",
		})
		w.Write(response)
		return
	}

	err = h.mappingService.Map(r.Context(), mapping)
	if err != nil {
		http.Error(w, "Create mapping error", http.StatusInternalServerError)
		logrus.Error("Mapping - mapSegments - Create mapping error:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "internal server error",
		})
		w.Write(response)
		return
	}

	response, _ := json.Marshal(map[string]string{
		"message": "success",
	})
	w.Write(response)

	w.WriteHeader(http.StatusOK)
}
