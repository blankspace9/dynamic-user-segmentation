package rest

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/service"
)

type Handler struct {
	userService    service.User
	segmentService service.Segment
	mappingService service.Mapping
	historyService service.History
}

func NewHandler(services service.Services) *Handler {
	return &Handler{
		userService:    services.User,
		segmentService: services.Segment,
		mappingService: services.Mapping,
		historyService: services.History,
	}
}

func (h *Handler) InitRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/segment", h.newSegment).Methods(http.MethodPost)
	r.HandleFunc("/segment", h.deleteSegment).Methods(http.MethodDelete)
	r.HandleFunc("/map", h.getSegments).Methods(http.MethodGet)
	r.HandleFunc("/map", h.mapSegments).Methods(http.MethodPost)
	r.HandleFunc("/report", h.getReport).Methods(http.MethodGet)
	r.HandleFunc("/files/{filename}", h.getFile).Methods(http.MethodGet)

	return r
}
