package rest

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
)

func (h *Handler) newSegment(w http.ResponseWriter, r *http.Request) {
	var segment domain.Segment

	d := json.NewDecoder(r.Body)
	err := d.Decode(&segment)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		logrus.Error("Segments - newSegment - Failed to parse JSON:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "bad request",
		})
		w.Write(response)
		return
	}

	id, err := h.segmentService.CreateSegment(r.Context(), segment)
	if err != nil {
		http.Error(w, "Create segment error", http.StatusInternalServerError)
		logrus.Error("Segments - newSegment - Create segment error:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "internal server error",
		})
		w.Write(response)
		return
	}

	response, err := json.Marshal(map[string]int64{
		"id": id,
	})
	if err != nil {
		http.Error(w, "Failed to encode response JSON", http.StatusInternalServerError)
		logrus.Error("Segments - newSegment - Failed to encode response JSON:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "internal server error",
		})
		w.Write(response)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) deleteSegment(w http.ResponseWriter, r *http.Request) {
	var segment domain.Segment

	err := json.NewDecoder(r.Body).Decode(&segment)
	if err != nil {
		http.Error(w, "Failed to parse JSON", http.StatusBadRequest)
		logrus.Error("Segments - deleteSegment - Failed to parse JSON:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "bad request",
		})
		w.Write(response)
		return
	}

	err = h.segmentService.DeleteSegment(r.Context(), segment.Title)
	if err != nil {
		http.Error(w, "Delete segment error", http.StatusInternalServerError)
		logrus.Error("Segments - deleteSegment - Delete segment error:", err)
		response, _ := json.Marshal(map[string]string{
			"message": "internal server error",
		})
		w.Write(response)
		return
	}

	response, _ := json.Marshal(map[string]string{
		"message": "success",
	})
	w.Write(response)

	w.WriteHeader(http.StatusOK)
}
