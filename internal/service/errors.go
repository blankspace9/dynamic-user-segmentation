package service

import (
	"errors"
)

var (
	ErrUserAlreadyExists = errors.New("user already exists")
	ErrCannotCreateUser  = errors.New("cannot create user")
	ErrUserNotFound      = errors.New("user not found")
	ErrCannotGetUser     = errors.New("cannot get user")

	ErrSegmentAlreadyExists = errors.New("segment already exists")
	ErrCannotCreateSegment  = errors.New("cannot create segment")
	ErrSegmentNotFound      = errors.New("segment not found")
	ErrCannotGetSegment     = errors.New("cannot get segment")

	ErrMappingAlreadyExists = errors.New("mapping already exists")
	ErrCannotCreateMapping  = errors.New("cannot create mapping")
	ErrMappingNotFound      = errors.New("mapping not found")
	ErrCannotGetMapping     = errors.New("cannot get mapping")
)
