package service

import (
	"context"
	"encoding/csv"
	"fmt"
	"os"

	"github.com/google/uuid"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
)

type HistoryService struct {
	historyRepo repository.History

	port string
}

func NewHistoryService(historyRepo repository.History, port string) *HistoryService {
	return &HistoryService{historyRepo, port}
}

func (hs *HistoryService) GetReport(ctx context.Context, req domain.HistoryRequest) (string, error) {
	logs, err := hs.historyRepo.Get(ctx, req)
	if err != nil {
		return "", err
	}

	err = os.MkdirAll("./files", os.ModePerm)
	if err != nil {
		return "", err
	}

	file, err := os.Create(fmt.Sprintf("files/user%d-%d-%d-"+uuid.New().String()+".csv", req.UserId, req.Year, req.Month))
	if err != nil {
		return "", err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	writer.Comma = ';'

	header := []string{"Segment", "Action", "Datetime"}
	writer.Write(header)

	for _, log := range logs {
		if err := writer.Write([]string{log.Segment, log.Action, log.Date.Format("2006-01-02 15:04:05")}); err != nil {
			return "", err
		}
	}
	writer.Flush()

	return "http://localhost:" + hs.port + "/" + file.Name(), nil
}
