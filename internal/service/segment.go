package service

import (
	"context"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type SegmentService struct {
	segmentRepo repository.Segment
}

func NewSegmentService(segmentRepo repository.Segment) *SegmentService {
	return &SegmentService{segmentRepo}
}

func (ss *SegmentService) CreateSegment(ctx context.Context, segment domain.Segment) (int64, error) {
	id, err := ss.segmentRepo.Create(ctx, segment)
	if err != nil {
		if err == repoerrors.ErrAlreadyExist {
			return -1, ErrSegmentAlreadyExists
		}
		return -1, ErrCannotCreateSegment
	}

	return id, nil
}

func (ss *SegmentService) DeleteSegment(ctx context.Context, title string) error {
	return ss.segmentRepo.Delete(ctx, title)
}
