package service

import (
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
)

type TTLService struct {
	mappingRepo repository.Mapping
	interval    time.Duration
	stopChan    chan struct{}
}

func NewTTLService(mappingRepo repository.Mapping, interval time.Duration) *TTLService {
	return &TTLService{
		mappingRepo: mappingRepo,
		interval:    interval,
		stopChan:    make(chan struct{}),
	}
}

func (s *TTLService) Start() {
	ticker := time.NewTicker(s.interval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			err := s.mappingRepo.DeleteExpired()
			if err != nil {
				log.Errorf("Delete expired error: %s", err)
			}
			log.Info("Expired maps deleted")
		case <-s.stopChan:
			return
		}
	}
}

func (s *TTLService) Stop() {
	log.Infof("Stop TTL manager")
	close(s.stopChan)
}
