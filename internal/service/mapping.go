package service

import (
	"context"
	"time"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type MappingService struct {
	userRepo    repository.User
	segmentRepo repository.Segment
	mappingRepo repository.Mapping
	historyRepo repository.History
}

func NewMappingService(userRepo repository.User, segmentRepo repository.Segment, mappingRepo repository.Mapping, historyRepo repository.History) *MappingService {
	return &MappingService{userRepo, segmentRepo, mappingRepo, historyRepo}
}

func (ms *MappingService) Map(ctx context.Context, mapping domain.Mapping) error {
	_, err := ms.userRepo.Create(ctx, domain.User{Id: mapping.UserId})
	if err != nil && err != repoerrors.ErrAlreadyExist {
		return err
	}

	for _, segment := range mapping.NewSegments {
		ttl, err := time.ParseDuration(mapping.TTL)
		if err != nil {
			return err
		}

		err = ms.mappingRepo.Create(ctx, mapping.UserId, segment, ttl)
		if err != nil && err != repoerrors.ErrAlreadyExist {
			return err
		}
		err = ms.historyRepo.Create(ctx, domain.HistoryLog{
			UserId:  mapping.UserId,
			Segment: segment,
			Action:  "add",
			Date:    time.Now()})
		if err != nil {
			return err
		}
	}

	for _, segment := range mapping.CanelledSegments {
		err := ms.mappingRepo.Delete(ctx, mapping.UserId, segment)
		if err != nil {
			return err
		}
		err = ms.historyRepo.Create(ctx, domain.HistoryLog{
			UserId:  mapping.UserId,
			Segment: segment,
			Action:  "delete",
			Date:    time.Now()})
		if err != nil {
			return err
		}
	}

	return nil
}

func (ms *MappingService) GetSegmentsByUser(ctx context.Context, userId int64) ([]string, error) {
	segments, err := ms.mappingRepo.GetSegmentsByUser(ctx, userId)
	if err != nil {
		if err == repoerrors.ErrNotFound {
			return nil, ErrMappingNotFound
		}
		return nil, ErrCannotGetMapping
	}

	return segments, nil
}
