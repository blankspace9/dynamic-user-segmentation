package service

import (
	"context"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository/repoerrors"
)

type UserService struct {
	userRepo repository.User
}

func NewUserService(userRepo repository.User) *UserService {
	return &UserService{userRepo}
}

func (us *UserService) CreateUser(ctx context.Context, user domain.User) error {
	_, err := us.userRepo.Create(ctx, user)
	if err != nil {
		if err == repoerrors.ErrAlreadyExist {
			return ErrUserAlreadyExists
		}
		return ErrCannotCreateUser
	}

	return nil
}
