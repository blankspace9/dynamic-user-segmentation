package service

import (
	"context"
	"time"

	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/domain"
	"gitlab.com/blankspace9/dynamic-user-segmentation/internal/repository"
)

type User interface {
	CreateUser(ctx context.Context, user domain.User) error
}

type Segment interface {
	CreateSegment(ctx context.Context, segment domain.Segment) (int64, error)
	DeleteSegment(ctx context.Context, title string) error
}

type Mapping interface {
	Map(ctx context.Context, mapping domain.Mapping) error
	GetSegmentsByUser(ctx context.Context, userId int64) ([]string, error)
}

type TTLManager interface {
	Start()
	Stop()
}

type History interface {
	GetReport(ctx context.Context, req domain.HistoryRequest) (string, error)
}

type ServiceDependencies struct {
	Repos            *repository.Repositories
	TTLCheckInterval time.Duration
	Port             string
}

type Services struct {
	User
	Segment
	Mapping
	TTLManager
	History
}

func NewServices(deps ServiceDependencies) *Services {
	return &Services{
		User:       NewUserService(deps.Repos.User),
		Segment:    NewSegmentService(deps.Repos.Segment),
		Mapping:    NewMappingService(deps.Repos.User, deps.Repos.Segment, deps.Repos.Mapping, deps.Repos.History),
		TTLManager: NewTTLService(deps.Repos.Mapping, deps.TTLCheckInterval),
		History:    NewHistoryService(deps.Repos.History, deps.Port),
	}
}
