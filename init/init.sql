DROP TABLE IF EXISTS users, segments, user_segment_mapping, mapping_history;

CREATE TABLE users (
	id INTEGER PRIMARY KEY
);

CREATE TABLE segments (
	id SERIAL PRIMARY KEY,
	title VARCHAR(255) UNIQUE CHECK(title!='')
);

CREATE TABLE user_segment_mapping (
	user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
	segment_title VARCHAR(255) REFERENCES segments(title) ON DELETE CASCADE,
	expired_at TIMESTAMP,
	PRIMARY KEY (user_id, segment_title)
);

CREATE TABLE mapping_history (
	user_id INTEGER,
	segment_title VARCHAR(255),
	action VARCHAR(10),
	time TIMESTAMP NOT NULL
)