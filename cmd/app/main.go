package main

import "gitlab.com/blankspace9/dynamic-user-segmentation/internal/app"

const (
	CONFIG_DIR  = "configs"
	CONFIG_FILE = "config"
)

func main() {
	app.Run(CONFIG_DIR, CONFIG_FILE)
}
