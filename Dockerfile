FROM golang:alpine AS modules
WORKDIR /modules
COPY go.mod go.sum /modules/
RUN go mod download

FROM golang:alpine AS builder
COPY --from=modules /go/pkg /go/pkg
WORKDIR /app
COPY . /app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -tags migrate -o /bin/app ./cmd/app

FROM scratch
COPY --from=builder /bin/app /app
COPY --from=builder /app/configs /configs
CMD ["/app"]